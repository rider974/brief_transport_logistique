-- Créez une vue qui affiche les informations suivantes pour chaque entrepôt : nom de l'entrepôt, adresse complète, nombre d'expéditions envoyées au cours des 30 derniers jours.
CREATE VIEW vue_entrepot_data
AS 
SELECT ent.nom_entrepot as nomEntrepot, ent.adresse_entrepot, count(*) as NbExpeditionsAuCoursDes30DerniersJours
FROM entrepot ent 
LEFT JOIN expedition exp 
ON exp.id_entrepot_source = ent.id_entrepot
WHERE exp.date_expedition >  CURRENT_DATE() - INTERVAL 30 DAY
GROUP BY ent.id_entrepot;

-- Créez une procédure stockée qui prend en entrée l'ID d'un entrepôt et renvoie le nombre total d'expéditions envoyées par cet entrepôt au cours du dernier mois.

-- MySQL Syntax ON phpMyAdmin


CREATE PROCEDURE `procTotalExpEnt`(IN `id_ent` INT) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER 

SELECT count(*) AS nbTotalExp
FROM entrepot ent 
INNER JOIN expedition exp 
ON exp.id_entrepot_source = ent.id_entrepot
WHERE exp.id_entrepot_source = @id_ent;

-- Créez une fonction qui prend en entrée une date et renvoie le nombre total d'expéditions livrées ce jour-là.

DELIMITER //

CREATE FUNCTION numberExpByDate( dayDate date) RETURNS int DETERMINISTIC
BEGIN
  RETURN (select count(*) from expedition where date_livraison = dayDate group by date_livraison);
END 

//

DELIMITER ;

select numberExpByDate('2023-04-07');


-- Ajoutez une table "clients" contenant les colonnes suivantes :
-- id (entier auto-incrémenté, clé primaire) nom (chaîne de caractères) adresse (chaîne de caractères) ville (chaîne de caractères) pays (chaîne de caractères)

CREATE TABLE client (
    id_client INT AUTO_INCREMENT PRIMARY KEY, 
    nom_client VARCHAR(75) NOT NULL, 
    adresse_client VARCHAR(255),
    ville_client VARCHAR(75),
    pays_client VARCHAR(75)
    )ENGINE=INNODB;


-- Ajoutez une table de jointure "expeditions_clients" contenant les colonnes suivantes :
-- id_expedition (entier, clé étrangère faisant référence à la table "expeditions") id_client (entier, clé étrangère faisant référence à la table "clients")

CREATE TABLE `expeditions_clients`(
    `id_expedition` INT NOT NULL,
    `id_client` INT NOT NULL,
    PRIMARY KEY (`id_expedition`, `id_client`),
    FOREIGN KEY (`id_expedition`) REFERENCES `expedition`(`id_expedition`),
    FOREIGN KEY (`id_client`) REFERENCES `client`(`id_client`)
)ENGINE=INNODB;
>>>>>>> Bonus.sql

-- Modifiez la table "expeditions" pour y ajouter une colonne "id_client" (entier, clé étrangère faisant référence à la table "clients").

ALTER TABLE expedition
ADD COLUMN id_client INT ;

ALTER TABLE expedition 
ADD CONSTRAINT fk_expedition_client FOREIGN KEY (id_client) REFERENCES client(id_client);

UPDATE expedition SET id_client = 2 WHERE id_expedition = 1;
UPDATE expedition SET id_client = 1 WHERE id_expedition = 2;
UPDATE expedition SET id_client = 3 WHERE id_expedition = 3;
UPDATE expedition SET id_client = 5 WHERE id_expedition = 4;
UPDATE expedition SET id_client = 1 WHERE id_expedition = 5;
UPDATE expedition SET id_client = 5 WHERE id_expedition = 6;
UPDATE expedition SET id_client = 1 WHERE id_expedition = 7;
UPDATE expedition SET id_client = 3 WHERE id_expedition = 8;
UPDATE expedition SET id_client = 1 WHERE id_expedition = 9;
UPDATE expedition SET id_client = 4 WHERE id_expedition = 10;

-- Ajoutez des données aux tables "clients" et "expeditions_clients". **Écrivez des requêtes pour extraire les informations suivantes : **- Pour chaque client, affichez son nom, son adresse complète, le nombre total d'expéditions qu'il a envoyées et le nombre total d'expéditions qu'il a reçues.

INSERT INTO client (nom_client, adresse_client, ville_client, pays_client)
VALUES('Bertrand Leger-Pasquier', 'impasse Raymond Royer', 'PhilippeVille', 'France'),
('Jean de la Pierre', 'rue Adèle Pinto', 'Jacques-sur-Pichon', 'France'),
('井上 知実', '宇野町村山7-1-3', '村山市', 'Japon'),
('Lukas Vandamme', 'impasse Bogaerts 9', 'Beaumont', 'Allemagne'),
('Miguel Angel', 'calle de la Cruz 3', 'Madrid', 'Espagne');

INSERT INTO expeditions_clients (id_expedition, id_client)
VALUES(1,1),
(2,3),
(3,2),
(4,4),
(5,5),
(6,1),
(7,3),
(8,2),
(9,4),
(10,5);


-- Pour chaque client, affichez son nom, son adresse complète, le nombre total d'expéditions qu'il a envoyées et le nombre total d'expéditions qu'il a reçues.

-- SELECT cli.nom_client, cli.adresse_client, cli.ville_client, cli.pays_client, count(expCli.id_client) as nbTotalExpeditionsEnvoyees, count(exp.id_client) as nbTotalExpeditionsRecues
-- FROM client cli
-- INNER JOIN expeditions_clients expCli
-- ON expCli.id_client = cli.id_client
-- LEFT JOIN expedition exp
-- ON exp.id_client = cli.id_client
-- GROUP BY cli.id_client;

-- Pour chaque expédition, affichez son ID, son poids, le nom du client qui l'a envoyée, le nom du client qui l'a reçue et le statut

SELECT exp.id_expedition, exp.poids_expedition, clientExp.nom_client, clientDest.nom_client, exp.statut_expedition
FROM expedition exp 
INNER JOIN expeditions_clients exp_cli 
ON exp_cli.id_expedition = exp.id_expedition
INNER JOIN client clientExp 
ON clientExp.id_client = exp_cli.id_client
INNER JOIN client clientDest 
ON clientDest.id_client = exp.id_client;