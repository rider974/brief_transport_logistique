-- Affichez les expéditions en transit qui ont été initiées par un entrepôt situé en Europe et à destination d'un entrepôt situé en Asie. Marche pas

CREATE TABLE IF NOT EXISTS `continent`(
	`id_continent` INT AUTO_INCREMENT PRIMARY KEY,
    `nom_continent` VARCHAR(50) NOT NULL,
    `array_pays` JSON
);

INSERT INTO `continent`(`nom_continent`, `array_pays`) VALUES
('Amérique', '["Cuba","Mexique","USA", "Canada", "Colombie", "Brézil"]'),
('Europe', '["Allemagne", "Italie", "Royaume-Uni", "France", "Grèce", "Pays-Bas", "Suisse", "Ukraine", "Pologne", "Belgique", "Espagne"]'),
('Asie', '["Japon", "Indonésie", "Chine", "Inde", "Corée du Sud", "Thaïlande", "Philippines", "Singapour", "Vietnam", "Laos", "Cambodge"]'),
('Océanie', '["Nouvelle-Zélande", "Australie", "Fidji", "Micronésie", "Samoa", "Vanuatu", "Kiribati", "Tonga"]'),
('Afrique', '["Afrique du Sud", "Nigeria", "Kenya", "Ghana", "Maroc", "Sénégal", "Tanzanie", "Mali"]');

UPDATE `entrepot` SET `ville_entrepot` = 'Tokyo', `pays_entrepot` = 'Japon' WHERE `entrepot`.`id_entrepot` = 2;

SELECT ex.id_expedition, c.`nom_continent` FROM `expedition` AS ex INNER JOIN `entrepot` AS en ON ex.`id_entrepot_source` = en.`id_entrepot` LEFT JOIN `continent` AS c ON JSON_UNQUOTE(JSON_EXTRACT(c.array_pays, '$')) = en.pays_entrepot WHERE ex.statut_expedition = 'transit';

SELECT en.nom_entrepot, en.pays_entrepot, c.nom_continent FROM `entrepot` AS en INNER JOIN `continent` AS c ON JSON_UNQUOTE(JSON_EXTRACT(c.array_pays, '$')) = en.pays_entrepot;

-- Affichez les entrepôts qui ont envoyé des expéditions à destination d'un entrepôt situé dans le même pays.

SELECT distinct entSource.* 
FROM `entrepot` entSource
INNER JOIN expedition exp 
ON exp.id_entrepot_source = entSource.id_entrepot
INNER JOIN entrepot entDest 
ON entDest.id_entrepot = exp.id_entrepot_destination
WHERE entSource.pays_entrepot = entDest.pays_entrepot;

-- Affichez les entrepôts qui ont envoyé des expéditions à destination d'un entrepôt situé dans un pays différent.

SELECT distinct entSource.* 
FROM `entrepot` entSource
INNER JOIN expedition exp ON exp.id_entrepot_source = entSource.id_entrepot
INNER JOIN entrepot entDest ON entDest.id_entrepot = exp.id_entrepot_destination
WHERE entSource.pays_entrepot != entDest.pays_entrepot;

-- Affichez les expéditions en transit qui ont été initiées par un entrepôt situé dans un pays dont le nom commence par la lettre "F" et qui pèsent plus de 500 kg.

SELECT exp.*
FROM `expedition` exp
INNER JOIN entrepot entSource
ON entSource.id_entrepot = exp.id_entrepot_source
WHERE entSource.pays_entrepot LIKE "F%"
AND `statut_expedition` = "en transit"
AND `poids_expedition` > 500;

-- Affichez le nombre total d'expéditions pour chaque combinaison de pays d'origine et de destination.

-- affiche les combinaisons de pays qui ont echange 
-- les combinaisons qui n'ont  pas fait d'échange ne sont pas pris en compte 

SELECT entSource.pays_entrepot, entDest.pays_entrepot, count(*) as nbExpeditions
FROM `expedition` exp
LEFT JOIN entrepot entSource
ON entSource.id_entrepot = exp.id_entrepot_source
LEFT JOIN entrepot entDest
ON entDest.id_entrepot = exp.id_entrepot_destination
GROUP BY entSource.pays_entrepot, entDest.pays_entrepot;

-- Affichez les entrepôts qui ont envoyé des expéditions au cours des 30 derniers jours et dont le poids total des expéditions est supérieur à 1000 kg.

SELECT entSource.*
FROM `expedition` exp 
INNER JOIN entrepot entSource 
ON entSource.id_entrepot = exp.id_entrepot_source
WHERE month(exp.date_expedition) = month(CURRENT_DATE)
GROUP BY entSource.id_entrepot
HAVING SUM(exp.poids_expedition) > 1000;

-- Affichez les expéditions qui ont été livrées avec un retard de plus de 2 jours ouvrables. modif (vérifier dans ce cas si la différence entre la date d 'expédition et la date de livraison est supérieur à 2 jours)

SELECT id_expedition from expedition where statut_expedition = "livrer" and DATEDIFF(date_livraison ,date_expedition) > 2;


-- Affichez le nombre total d'expéditions pour chaque jour du mois en cours, trié par ordre décroissant
SELECT day(date_expedition) as JourDuMoisEnCours, count(*) as nbExpeditions
FROM expedition  
WHERE month(date_expedition) = month(CURRENT_DATE)
GROUP BY JourDuMoisEnCours
ORDER BY 2 desc ;

