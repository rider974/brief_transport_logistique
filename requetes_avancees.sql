-- Affichez les entrepôts qui ont envoyé au moins une expédition en transit.


SELECT ent.* 
FROM `entrepot`ent 
INNER JOIN expedition ex 
ON ex.id_entrepot_source = ent.`id_entrepot` 
WHERE ex.statut_expedition = 'en transit'
GROUP BY ent.`id_entrepot`;


-- Affichez les entrepôts qui ont reçu au moins une expédition en transit.


SELECT ent.* 
FROM `entrepot`ent 
INNER JOIN expedition ex 
ON ex.id_entrepot_destination = ent.`id_entrepot` 
WHERE ex.statut_expedition = 'en transit'
GROUP BY ent.`id_entrepot`;

-- Affichez les expéditions qui ont un poids supérieur à 100 kg et qui sont en transit.


SELECT * 
FROM `expedition` 
WHERE `poids_expedition` > 100 
AND `statut_expedition` = 'en transit';


-- Affichez le nombre d'expéditions envoyées par chaque entrepôt.

SELECT ent.*, count(*) as nbEnvoi
FROM `entrepot`ent 
INNER JOIN expedition ex 
ON ex.id_entrepot_source = ent.`id_entrepot`
GROUP BY ent.`id_entrepot`;


-- Affichez le nombre total d'expéditions en transit.

SELECT  count(*) as nbExpeditionEnTransit
FROM  expedition 
WHERE `statut_expedition` = 'en transit';

-- Affichez le nombre total d'expéditions livrées.

SELECT  count(*) as nbExpeditionLivrees
FROM  expedition 
WHERE `statut_expedition` = 'livrer';

-- Affichez le nombre total d'expéditions pour chaque mois de l'année en cours.

SELECT  month(`date_expedition`) as MoisEnCours, count(*) as nbExpedition
FROM expedition 
GROUP BY month(`date_expedition`)
ORDER BY MoisEnCours;

-- Affichez les entrepôts qui ont envoyé des expéditions au cours des 30 derniers jours.
SELECT ent.* 
FROM `expedition`exp 
INNER JOIN entrepot ent 
ON ent.id_entrepot = exp.`id_entrepot_source` 
WHERE `date_expedition` > CURRENT_DATE() - INTERVAL 30 DAY;

-- Affichez les entrepôts qui ont reçu des expéditions au cours des 30 derniers jours.

SELECT ent.* 
FROM `expedition`exp 
INNER JOIN entrepot ent 
ON ent.id_entrepot = exp.`id_entrepot_destination` 
WHERE `date_expedition` > CURRENT_DATE() - INTERVAL 30 DAY;

-- Affichez les expéditions qui ont été livrées dans un délai de moins de 5 jours ouvrables.

SELECT expedition.*
FROM `expedition` 
WHERE date_livraison -`date_expedition`  <5;