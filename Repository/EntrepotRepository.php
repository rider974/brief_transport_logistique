<?php 


class EntrepotRepository extends Bdd
{

    /************************DEBUT CRUD *********************************** */

/**
 * create a new entrepot 
 *
 * create a entrepot by giving the element in a function 
 *
 * @param Type $nom string, $adresse string, $ville string, $pays string
 * @throws condition
 **/
    public function createEntrepot(string $nom, string $adresse, string $ville, string $pays)
    {
        $db = $this->getDb();
        
        $sql = $db->prepare('INSERT INTO entrepot VALUES (null,:nom,:adresse, :ville, :pays) ');

        $sql->bindParam(":nom" ,$nom);
        
        $sql->bindParam(":adresse" ,$adresse);
        
        $sql->bindParam(":ville" ,$ville);
        
        $sql->bindParam(":pays" ,$pays);

        $sql->execute(); 
    } 

    
/**
 * create a new entrepot 
 *
 * create a entrepot by giving the element in a function 
 *
 * @param Type $entrepot Entrepot 
 * @throws condition
 **/
public function createEntrepotObject(Entrepot $entrepot)
{
    $db = $this->getDb();
    
    $sql = $db->prepare('INSERT INTO entrepot VALUES (null,:nom,:adresse, :ville, :pays) ');

    $sql->bindParam(":nom" ,$nom);
    
    $sql->bindParam(":adresse" ,$adresse);
    
    $sql->bindParam(":ville" ,$ville);
    
    $sql->bindParam(":pays" ,$pays);

    $sql->execute(); 
} 




    /**
     * Delete the entrepot in parameter
     *
     *
     * @param Type $entrepot Entrepot
     * @return nothing
     * @throws conditon
     **/
    public function deleteEntrepot(Entrepot $entrepot)
    {
        $db = $this->getDb();
        $idEntrepot = $entrepot->getId();

        $query = "DELETE FROM entrepot WHERE id_entrepot = :idC";

        $query_del = $db->prepare($query);


        $query_del ->bindParam(":idC", $idEntrepot);

        $querydel->execute();
    }


    /**
     * get the entrepot with the id in param
     *
     *
     * @param Type $idEntrepot Int 
     * @return Array
     * @throws conditon
     **/
    public function getEntrepot(int $idEntrepot)   
     {
        
       $db = $this->getDb();
       $query = "SELECT * FROM entrepot WHERE id_entrepot = :idC";

       $get_entrepot = $db->prepare($query);


       $get_entrepot->bindParam(":idC", $idEntrepot);



       $get_entrepot->execute();

       $entrepot = $get_entrepot->fetch();


       return $entrepot; 
    }


    /**
     * update the entrepot 
     *
     *
     * @param Type int $idC, string $nom, string $adresse, string $ville, string $pays
     * @return nothing
     * @throws conditon
     **/
    public function updateEntrepot(int $idC, string $nom, string $adresse, string $ville, string $pays)
    {
        $db = $this->getDb();
        $sql = $db->prepare('UPDATE entrepot SET nom_entrepot = ?, adresse_entrepot = ?, ville_entrepot = ?, pays_entrepot = ? WHERE id_entrepot = ?');
        $sql->bindParam(1, $nom);
        $sql->bindParam(2 ,$adresse);
        $sql->bindParam(3 ,$ville);
        $sql->bindParam(4 ,$pays);
        $sql->bindParam(5, $idC);
        $sql->execute(); 
    }

    /******************************FIN CRUD ******************************* */



    /**
     * get All Entrepot
     *
     * Undocumented function long description
     *
     * @return Array of Entrepots 
     * @throws conditon
     **/
    public function getAllEntrepots()
    {
        // Affichez tous les entrepôts.

        $db = $this->getDb();
       $query = " SELECT * FROM `entrepot`";


       $get_all_entrepot = $db->prepare($query);

       $get_all_entrepot->execute();

       $lesEntrepot = $get_all_entrepot->fetchAll();


       return $lesEntrepot;
    }

    /**
     * get all the entrepot which has sent at least 1 expedition in transit 
     *
     * The entrepot is the sender 
     * 
     * @return Array Entrepots
     * @throws conditon
     **/
    public function getEntWithtransitExp()
    {

        $db = $this->getDb();

        $query = "SELECT ent.* 
                    FROM `entrepot`ent 
                    INNER JOIN expedition ex 
                    ON ex.id_entr   epot_source = ent.`id_entrepot` 
                    WHERE ex.statut_expedition = 'en transit'
                    GROUP BY ent.`id_entrepot`";

        $getEntrepots = $db->prepare($query);

        $getEntrepots->execute();

        $Entrepots = $getEntrepots>fetchAll();

        return $Entrepots;

        
    }


  /**
     * get all the entrepot which send expeditions in the same country 
     *
     * @return Array Entrepots
     * @throws conditon
     **/
    public function getEntSendSameCountry()
    {

        $db = $this->getDb();
    
//  Affichez les entrepôts qui ont envoyé des expéditions à destination d'un entrepôt situé dans le même pays.

        $query = "SELECT distinct entSource.* 
                    FROM `entrepot` entSource
                    INNER JOIN expedition exp 
                    ON exp.id_entrepot_source = entSource.id_entrepot
                    INNER JOIN entrepot entDest 
                    ON entDest.id_entrepot = exp.id_entrepot_destination
                    WHERE entSource.pays_entrepot = entDest.pays_entrepot";


        $getEntrepots = $db->prepare($query);

        $getEntrepots->execute();

        $Entrepots = $getEntrepots>fetchAll();

        return $Entrepots;
    }

    /**
     * get all the entrepot which send expeditions in adifferent country 
     *
     * @return Array Entrepots
     * @throws conditon
     **/
    public function getEntSendOtherCountry()
    {

        $db = $this->getDb();
    
//  Affichez les entrepôts qui ont envoyé des expéditions à destination d'un entrepôt situé dans le même pays.

        $query = "SELECT distinct entSource.* 
                    FROM `entrepot` entSource
                    INNER JOIN expedition exp 
                    ON exp.id_entrepot_source = entSource.id_entrepot
                    INNER JOIN entrepot entDest 
                    ON entDest.id_entrepot = exp.id_entrepot_destination
                    WHERE entSource.pays_entrepot != entDest.pays_entrepot";


        $getEntrepots = $db->prepare($query);

        $getEntrepots->execute();

        $Entrepots = $getEntrepots>fetchAll();

        return $Entrepots;
    }


    /**
     * get all the entrepot which send expeditions during last 30 days and with weight of more than 1000kg 
     *
     * @return Array Entrepots
     * @throws conditon
     **/
    public function getEnt30DaysSup1000W()
    {

        $db = $this->getDb();
    
// Affichez les entrepôts qui ont envoyé des expéditions au cours des 30 derniers jours et dont le poids total des expéditions est supérieur à 1000 kg.

        $query = "SELECT entSource.*
                    FROM `expedition` exp 
                    INNER JOIN entrepot entSource 
                    ON entSource.id_entrepot = exp.id_entrepot_source
                    WHERE month(exp.date_expedition) = month(CURRENT_DATE)
                    GROUP BY entSource.id_entrepot
                    HAVING SUM(exp.poids_expedition) > 1000;";


        $getEntrepots = $db->prepare($query);

        $getEntrepots->execute();

        $Entrepots = $getEntrepots>fetchAll();

        return $Entrepots;
    }


}