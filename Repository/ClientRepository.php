<?php 


class ClientRepository extends Bdd
{

    /************************DEBUT CRUD *********************************** */

/**
 * create a new client 
 *
 * create a client by giving the element in a function 
 *
 * @param Type $nom string, $adresse string, $ville string, $pays string
 * @throws condition
 **/
    public function createClient(string $nom, string $adresse, string $ville, string $pays)
    {
        $db = $this->getDb();
        
        $sql = $db->prepare('INSERT INTO client VALUES (null,:nom,:adresse, :ville, :pays) ');

        $sql->bindParam(":nom" ,$nom);
        
        $sql->bindParam(":adresse" ,$adresse);
        
        $sql->bindParam(":ville" ,$ville);
        
        $sql->bindParam(":pays" ,$pays);

        $sql->execute(); 
    } 

    
/**
 * create a new client 
 *
 * create a client by giving the element in a function 
 *
 * @param Type $client Client 
 * @throws condition
 **/
public function createClientObject(Client $client)
{
    $db = $this->getDb();
    
    $sql = $db->prepare('INSERT INTO client VALUES (null,:nom,:adresse, :ville, :pays) ');

    $sql->bindParam(":nom" ,$nom);
    
    $sql->bindParam(":adresse" ,$adresse);
    
    $sql->bindParam(":ville" ,$ville);
    
    $sql->bindParam(":pays" ,$pays);

    $sql->execute(); 
} 




    /**
     * Delete the client in parameter
     *
     *
     * @param Type $client Client
     * @return nothing
     * @throws conditon
     **/
    public function deleteClient(Client $client)
    {
        $db = $this->getDb();
        $idClient = $client->getId();

        $query = "DELETE FROM client WHERE id_client = :idC";

        $query_del = $db->prepare($query);


        $query_del ->bindParam(":idC", $idClient);

        $querydel->execute();
    }


    /**
     * get the client with the id in param
     *
     *
     * @param Type $idClient Int 
     * @return Array
     * @throws conditon
     **/
    public function getClient(int $idClient)   
     {
        
       $db = $this->getDb();
       $query = "SELECT * FROM client WHERE id_client = :idC";

       $get_client = $db->prepare($query);


       $get_client->bindParam(":idC", $idClient);



       $get_client->execute();

       $client = $get_client->fetch();


       return $client; 
    }


    /**
     * update the client 
     *
     *
     * @param Type int $idC, string $nom, string $adresse, string $ville, string $pays
     * @return nothing
     * @throws conditon
     **/
    public function updateClient(int $idC, string $nom, string $adresse, string $ville, string $pays)
    {
        $db = $this->getDb();
        $sql = $db->prepare('UPDATE client SET nom_client = ?, adresse_client = ?, ville_client = ?, pays_client = ? WHERE id_client = ?');
        $sql->bindParam(1, $nom);
        $sql->bindParam(2 ,$adresse);
        $sql->bindParam(3 ,$ville);
        $sql->bindParam(4 ,$pays);
        $sql->bindParam(5, $idC);
        $sql->execute(); 
    }

    /******************************FIN CRUD ******************************* */



    
}