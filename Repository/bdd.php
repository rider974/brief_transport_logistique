<?php 


abstract class Bdd
{
  private static $dbh;

  protected function getDb(){
    if($this->dbh === null){
      self::dbConnect();
    }
    return self::$dbh;
  }

  private static function dbConnect()
  {

    try {
      self::$dbh = new PDO("mysql:host=127.0.0.1;dbname=transport_logistique", $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
    } catch (PDOException $e) {
      echo 'Connexion échouée : ' . $e->getMessage();
    }
  }
}
