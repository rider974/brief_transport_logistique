<?php 

class expeditionRepository extends Bdd
{
    public function createExpedition($date_expedition, $date_livraison, $entre_src, $entre_dest, $pds, $status)
    {
        $db = $this->getDb();
        $sql = $db->prepare('INSERT INTO expedition (date_expedition, date_livraison, id_entrepot_source, id_entrepot_destination, poids_expedition, statut_expedition) VALUES (?,?,?,?,?,?)');
        $sql->bindParam(1, $date_expedition);
        $sql->bindParam(2 ,$date_livraison);
        $sql->bindParam(3 ,$entre_src);
        $sql->bindParam(4 ,$entre_dest);
        $sql->bindParam(5, $pds);
        $sql->bindParam(6, $status);

        $sql->execute(); 
    }

    public function deleteExpedition($id){
        $db=$this->getDb();
        $sql = $db->prepare('DELETE FROM expedition WHERE id_expedition = ?');
        $sql->bindParam(1, $id);
        $sql->execute();
    }

    public function readExpedition($id){
        $db=$this->getDb();
        $sql = $db->prepare('SELECT * FROM expedition where id_expedition = ?');
        $sql->bindParam(1, $id);
        $sql->execute();
    }

    public function updateExpedition($id, $date_expedition, $date_livraison, $entre_src, $entre_dest, $pds, $status)
    {
        $db = $this->getDb();
        $sql = $db->prepare('UPDATE expedition SET date_expedition = ?, date_livraison = ?, id_entrepot_source = ?, id_entrepot_destination = ?, poids_expedition = ?, statut_expedition =? Where id_expedition = ?');
        $sql->bindParam(1, $date_expedition);
        $sql->bindParam(2 ,$date_livraison);
        $sql->bindParam(3 ,$entre_src);
        $sql->bindParam(4 ,$entre_dest);
        $sql->bindParam(5, $pds);
        $sql->bindParam(6, $status);
        $sql->bindParam(7, $id);
        $sql->execute(); 
    }
}