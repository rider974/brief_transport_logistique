<H1>BRIEF Transport Logistique </H1>

<h2>Pré requis:<h2>

=> Avoir installé Mysql 

=> AVoir un SGBD compatible comme SQLite, WorkBench, PhpMyAdmin (fait partie de la suite Wampp| Xampp| Mampp)


<h2>Démarches:</h2>


=> faire un git clone https://gitlab.com/rider974/brief_transport_logistique.git

=> Dézipper le dossier 

=> Faire une copie de .env.example et le renommer en .env 

=> Ajouter votre user et password dans le .env

=> Aller sur votre SGBD

=> Cliquer sur Importer un fichier sql 

=> Selectionner le fichier database_creation_structure.sql 
(c'est lui qui va crée la BDD et les tables principales)

=> Aller dans votre BDD et importer <h3>EN PREMIER entrepot_data.sql </h3>

(ATTENTION!!! ce fichier doit être importer avant le second sinon l'importation ne passera pas) 

=> EN DEUXIEME le fichier expedition_data.sql


<h2>Requêtes:</h2>

=> Vous trouverez les requêtes dans les fichiers nommées requêtes...


<h3>Réalisé par Alexandre, Fidel, Kévin, Pascal, Simon</h3>
