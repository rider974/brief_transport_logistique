INSERT INTO expedition (id_expedition, date_expedition, date_livraison, id_entrepot_source, id_entrepot_destination, poids_expedition, statut_expedition) 
VALUES (1, '2023-04-04', null, 1, 2, '1000.00', 'en transit'), 
(2, '2023-04-04', '2023-04-07', 1, 3, '10000.52', 'livrer'), 
(3, '2023-01-24', null, 3, 4, '750.00', 'en transit'), 
(4, '2022-05-05', '2022-05-08', 4, 1, '1500.00', 'livrer'), 
(5, '2023-02-15', null, 2, 3, '56.80', 'en transit'), 
(6, '2023-03-16', '2023-03-20', 1, 4, '350.00', 'livrer'), 
(7, '2023-04-01', '2023-04-08', 3, 4, '1620.75', 'livrer'), 
(8, '2023-01-01', null, 4, 3, '650.34', 'en transit'), 
(9, '2022-02-12', '2022-02-15', 2, 3, '2600.00', 'livrer'), 
(10, '2023-03-01', null, 2, 4, '3000.00', 'en transit');