
-- création database 
CREATE DATABASE IF NOT EXISTS transport_logistique; 
USE transport_logistique;


-- création table entrepots  

CREATE TABLE IF NOT EXISTS entrepot (
    id_entrepot int auto_increment PRIMARY KEY ,
    nom_entrepot VARCHAR(50) NOT NULL, 
    adresse_entrepot VARCHAR(250),
    ville_entrepot VARCHAR(75),
    pays_entrepot VARCHAR(75)
)ENGINE=InnoDB;


-- création table expeditions 

CREATE TABLE IF NOT EXISTS expedition(
    id_expedition int auto_increment PRIMARY KEY,
    date_expedition DATE,
    date_livraison DATE,
    id_entrepot_source int,
    id_entrepot_destination int(5),
    poids_expedition decimal(8,2),
    statut_expedition VARCHAR(50)
)ENGINE=InnoDB;


-- foreign key entrepot source

ALTER TABLE expedition 
ADD CONSTRAINT fk_expedition_entrepot_source 
FOREIGN KEY  (id_entrepot_source) 
REFERENCES entrepot(id_entrepot);



-- foreign key entrepot destination

ALTER TABLE expedition 
ADD CONSTRAINT fk_expedition_entrepot_destination
FOREIGN KEY  (id_entrepot_destination) 
REFERENCES entrepot(id_entrepot);