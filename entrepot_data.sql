-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 04, 2023 at 11:50 AM
-- Server version: 8.0.27
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transport_logistique`
--

--
-- Dumping data for table `entrepot`
--

INSERT INTO `entrepot` (`id_entrepot`, `nom_entrepot`, `adresse_entrepot`, `ville_entrepot`, `pays_entrepot`) VALUES
(1, 'entrepot 1', '44 rue simplon ', 'Lyon', 'France'),
(2, 'entrepot 2', '2 rue de l\' insertion ', 'Paris', 'France'),
(3, 'entrepot 3', '25 rue du Bundestag ', 'Munich', 'Allemagne'),
(4, 'entrepot 4', '165 rue de la bière ', 'Bruxelles', 'Belgique'),
(5, 'entrepot 5', '56 rue flamenco', 'Madrid', 'Espagne');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
