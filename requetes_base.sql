-- Affichez tous les entrepôts.

SELECT * FROM `entrepot`;

-- Affichez toutes les expéditions.

SELECT * FROM `expedition`;

-- Affichez toutes les expéditions en transit.

SELECT * FROM `expedition` WHERE statut_expedition = 'en transit';

-- Affichez toutes les expéditions livrées.

SELECT * FROM `expedition` WHERE statut_expedition = 'livrer';